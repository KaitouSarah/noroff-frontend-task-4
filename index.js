//Task 4 code that became redundant with Task 5:
/* 
const http = require('http');

const server = http.createServer((request, response) => {
 response.write("Hello Node World!");
 response.end();
}).listen(8080);
*/

const express = require('express');

var app = express();

var path = require('path');

app.get('/', (req, res) => { res.sendFile(path.join(__dirname + '/index.html'))});

app.get('/someJavascript.js', (req, res) => { res.sendFile(path.join(__dirname + '/someJavascript.js'))});

app.listen(process.env.PORT || 8080);