# Noroff Frontend Task 4 and 5

<b>Running application can be found at: https://sarah-helloheroku.herokuapp.com/
______________________________________

<h3>Task 4 - Setup a node server

� Setup and run a node server with http

� Use npm

� The response should return �Hello Node World�

______________________________________

<h3>Task 5 - Setup an Express website

� Setup a node app with express

� Serve an index.html file

� The index file should have a h1 tag with �Hello Node World!!� as the text and a p tag with text of your choice.

� Add a JavaScript file

� Use console.log to display �Hello From an Express Site�

� You will need to use the sendFile() function to server your index
file
______________________________________
